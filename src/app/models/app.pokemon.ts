export interface Pokemon {
    id?: number;
    name: string;
    url: string;
    sprites?: PokemonSprite;
    abilities?: PokemonAbilitiesArray[];
    moves?: PokemonMovesArray[];
    stats?: PokemonStatsArray[];
    height?: number;
    weight?: number;
    base_experience?: number;

}

export interface PokemonShort {
    name: string;
    url: string;
}

export interface PokemonDTO {
    is_hidden: boolean;
    pokemon: Pokemon;
    slot: number;
}

export interface PokemonSprite {
    front_default: string;
    back_default: string;
}

export interface PokemonList {
    results: PokemonShort[]
}

// Abilities

export interface PokemonAbilitiesArray {
    ability: PokemonAbilityDTO;
}

export interface PokemonAbilityDTO {
    name: string;
    url: string;
}

export interface PokemonAbility {
    effect_changes: [];
    effect_entries: EffectEntry[];
    flavor_text_entries: [];
    generation: {};
    id: number;
    is_main_series: boolean;
    name: string;
    names: string[];
    pokemon: PokemonDTO[];
}

// Stats
export interface PokemonStatsArray {
    base_stat: number;
    effort: number;
    stat: PokemonStatDTO;
}

export interface PokemonStatDTO {
    name: string;
    url: string;
}

// Moves

export interface PokemonMovesArray {
    move: PokemonMoveDTO;
}

export interface PokemonMoveDTO {
    name: string;
    url: string;
}

export interface PokemonMove {
    name: string;
    accuracy: number;
    pp: number;
    priority: number;
    power: number;
    effect_entries: EffectEntry[];
}

// Effect Entries

export interface EffectLanguage {
    name: string;
    url: string;
}

export interface EffectEntry {
    effect: string;
    language: EffectLanguage;
    short_effect: string;
}