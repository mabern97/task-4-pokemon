import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  //public appTitle:string = "Mathias' Pokémon Collection";
  public appTitle: string = "";
}
