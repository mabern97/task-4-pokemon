import { Component, Input, OnInit } from '@angular/core';
import { Observable, timer } from 'rxjs';
import { Router } from '@angular/router';

import { Pokemon } from '../../../models/app.pokemon';

@Component({
  selector: 'app-grid-item',
  templateUrl: './grid-item.component.html',
  styleUrls: ['./grid-item.component.scss']
})
export class GridItemComponent implements OnInit {
  //@Input() Id: number;
  @Input() Pokemon: Pokemon;

  get Name(): string {
    return this.Pokemon.name;
  }

  get Sprite(): string {
    return this.Pokemon.sprites.front_default;
  }

  get Id(): number {
    return this.Pokemon.id;
  }

  constructor(private router: Router) {
  }

  ngOnInit(): void {

  }

  OnMouseClick() {
    this.router.navigateByUrl(`/details/${ this.Id }`);
  }
}
