import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './views/home/home.component'
import { PokemonDetailComponent } from './views/details/pokemon-detail/pokemon-detail.component';

import { AuthenticationGuard } from './guards/authentication.guard';
import { RegistrationComponent } from './views/registration/registration.component';
import { CollectionComponent } from './views/details/collection/collection.component';

const routes: Routes = [
  {
    path: 'registration',
    component: RegistrationComponent
  },
  {
    path: 'catalogue',
    component: HomeComponent,
    canActivate: [AuthenticationGuard]
  },
  {
    path: 'collection',
    component: CollectionComponent,
    canActivate: [AuthenticationGuard]
  },
  {
    path: 'details/:id',
    component: PokemonDetailComponent,
    canActivate: [AuthenticationGuard]
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/registration'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
