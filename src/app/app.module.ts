import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ActionbarComponent } from './components/actionbar/actionbar.component'

import { MaterialModule } from './material.module';
import { HomeComponent } from './views/home/home.component';
import { GridItemComponent } from './components/pokemon/grid-item/grid-item.component';
import { PokemonDetailComponent } from './views/details/pokemon-detail/pokemon-detail.component';
import { RegistrationComponent } from './views/registration/registration.component';
import { CollectionComponent } from './views/details/collection/collection.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    ActionbarComponent,
    HomeComponent,
    GridItemComponent,
    PokemonDetailComponent,
    RegistrationComponent,
    CollectionComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,

    // Material UI
    MaterialModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
