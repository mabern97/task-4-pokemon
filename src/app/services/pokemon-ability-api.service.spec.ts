import { TestBed } from '@angular/core/testing';

import { PokemonAbilityApiService } from './pokemon-ability-api.service';

describe('PokemonAbilityApiService', () => {
  let service: PokemonAbilityApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PokemonAbilityApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
