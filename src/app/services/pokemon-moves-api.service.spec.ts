import { TestBed } from '@angular/core/testing';

import { PokemonMovesApiService } from './pokemon-moves-api.service';

describe('PokemonMovesApiService', () => {
  let service: PokemonMovesApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PokemonMovesApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
